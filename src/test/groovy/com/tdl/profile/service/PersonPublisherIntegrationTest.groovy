package com.tdl.profile.service

import com.tdl.profile.IntegrationTest
import com.tdl.profile.config.RabbitConfig
import com.tdl.profile.model.Person
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired

class PersonPublisherIntegrationTest extends IntegrationTest {

  @Autowired
  PersonPublisher personPublisher

  @Autowired
  RabbitTemplate personAmqpTemplate

  def "Should publish person"() {

    given:
    Person person = readSamplePerson()

    when:
    personPublisher.publish(person)

    then:
    Person personQueued = personAmqpTemplate.receiveAndConvert(RabbitConfig.PERSON_QUEUE) as Person

    person == personQueued
  }

}
