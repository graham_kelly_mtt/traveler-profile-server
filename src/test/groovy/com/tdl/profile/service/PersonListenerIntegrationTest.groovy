package com.tdl.profile.service

import com.mttnow.platform.spring.boot.test.auto.configure.rabbit.setup.NeedsRabbitListener
import com.tdl.profile.IntegrationTest
import com.tdl.profile.config.RabbitConfig
import com.tdl.profile.model.Person
import org.spockframework.spring.SpringBean
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import spock.util.concurrent.AsyncConditions

class PersonListenerIntegrationTest extends IntegrationTest {

  @Autowired
  RabbitTemplate personAmqpTemplate

  @SpringBean
  PersonService personService = Mock()

  @NeedsRabbitListener(RabbitConfig.PERSON_QUEUE)
  def "Should consume Person"() {

    def asyncCondition = new AsyncConditions()

    given:
    Person person = readSamplePerson()

    personService.save(_ as Person) >> { Person argument ->
      asyncCondition.evaluate() {
        assert argument == person
      }
    }

    when:
    personAmqpTemplate.convertAndSend(person)

    then:
    asyncCondition.await()

  }


}
