package com.tdl.profile.service

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.tdl.profile.UnitTest
import com.tdl.profile.model.Person

class PersonListenerUnitTest extends UnitTest {

  @Subject
  PersonListener personListener

  @Collaborator
  PersonService personService = Mock()

  def "Should call save"() {

    given:
    def person = new Person(firstName: "John")

    when:
    personListener.consume(person)

    then:
    1 * personService.save(person)

  }

}
